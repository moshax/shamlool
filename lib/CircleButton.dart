import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final String icon;
  final String title;

  const CircleButton({Key key, this.onTap, this.icon,this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 95.0;

    return new InkResponse(
      onTap: onTap,
      child:
      new Container(
        padding: EdgeInsets.all(1),
        width: size,
        height: size,
        decoration:
        new BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Colors.blue,
              width: 1.5
          ),
          shape: BoxShape.circle,
        ),
        child:
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                ClipRRect(
                  //borderRadius: BorderRadius.circular(50.0),


                  child:
                  icon.startsWith("http")?
                  Image.network(
                    icon,
                    width: size-42,
                    height: size-42,
                  ):
                      Image.asset(icon,

                        width: size-30,
                        height: size-30,
                      )

                ),
                title==""?SizedBox(height: 1,):
                Container(
                  width: size-30,
                  child: Text(title, style: TextStyle(fontWeight: FontWeight.w800,fontSize: 10,),textAlign: TextAlign.center,),
                )
              ],
            )

      ),
    );
  }
}



class Dimension {
  const Dimension({this.right, this.left,this.top, this.bottom});

  final double right;
  final double left;
  final double top;
  final double bottom;


  factory Dimension.fromJson(Map<String, dynamic> json) {
    return Dimension(
      right: json['right'],
      left: json['left'],
      top: json['top'],
      bottom: json['bottom'],

    );
  }
}