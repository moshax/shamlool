//
// By ScriptStars, ScriptStars.com

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'constants.dart' as Constants;

class ContactUs extends StatefulWidget  {
  ContactUs({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ContactPageState createState() => new _ContactPageState();
}

class _ContactPageState extends State<ContactUs> {

    TextEditingController emailController = new TextEditingController();
    TextEditingController firstNameController = new TextEditingController();
    TextEditingController phoneController = new TextEditingController();
    TextEditingController messageController = new TextEditingController();
    String apiResponse;

    Future<String> submitContact() async {

      String urlParts="&firstName="+firstNameController.text
          + "&email="+emailController.text+ "&phone="
          +phoneController.text
          +"&message="+messageController.text;
      String url =Constants.BaseUrl+"?act=contact"+urlParts;
      //print(url);
      var response = await http.get(
          Uri.encodeFull(url),
          headers: {"Accept": "application/json"});

      setState(() {
        String jsonResponse = json.decode(response.body);
        apiResponse = jsonResponse;
        //form.reset();
        showAlertDialog(context);
      });

      return "Success";
    }
    showAlertDialog(BuildContext context) {

      // set up the button
      Widget okButton = FlatButton(
        child: Text("OK"),
        onPressed: () {
          Navigator.pop(context);
        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("شكرا لك"),
        content: Text("تم ارسال الرسالة بنجاح."),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }

    @override
    Widget build(BuildContext context) {
      final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
      void _submitForm() {
        final FormState form = _formKey.currentState;

        if (!form.validate()) {
          //showMessage('Form is not valid!  Please review and correct.');
        } else {
          form.save(); //This invokes each onSaved event

          submitContact();
          print('Form save called, newContact is now up to date...');
          print('Email: ${emailController.text}');


        }
      }
      return new Scaffold(
        appBar: new AppBar(
          title: new Text("اتصل بشملول"),
        ),
        body: new SafeArea(
            top: false,
            bottom: false,
            child: new Form(
                key: _formKey,
                autovalidate: true,
                child: new ListView(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  children: <Widget>[
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.person),
                        hintText: 'برجاء ادخال الاسم',
                        labelText: 'اسمك',
                      ),
                      controller: firstNameController,
                    ),
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.email),
                        hintText: 'برجاء كتابة البريد الالكتروني',
                        labelText: 'بريدك الالكتروني',
                      ),
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                    ),
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.phone),
                        hintText: 'برجاء كتابة رقم تليفون للتواصل',
                        labelText: 'الهاتف',
                      ),
                      keyboardType: TextInputType.phone,
                      controller: phoneController,
                      inputFormatters: [
//                        WhitelistingTextInputFormatter.digitsOnly,
                      ],
                    ),
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.sms),
                        hintText: 'اكتب رسالتك',
                        labelText: 'الرسالة',
                      ),
                      keyboardType: TextInputType.text,
                      controller: messageController,
                    ),

                    new Container(
                        padding: const EdgeInsets.only(right: 40.0, top: 20.0),
                        child: new RaisedButton(
                          child: const Text('ارسال'),
                          onPressed: _submitForm,
                        )
                    ),
                  ],
                ))),
      );
    }

  }

