//
// By ScriptStars, ScriptStars.com

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


import 'database/favorite_helper.dart';
import 'inner.dart';
import 'main.dart';

class Favorites extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    List posts = List();
    var db = FavoriteDB();


    return FutureBuilder<List>(
        future: db.listAll().then((all){
          posts.addAll(all);
          //print(posts.length.toString()+"hhh");
          return posts;
        }),
        builder: (context, AsyncSnapshot<List> snapshot) {
          if (snapshot.hasData) {
            //return Text(snapshot.data[0]);
            posts = snapshot.data;
            return Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: Text(
                  "المفضلة",
                ),
              ),
              body: posts.isEmpty
                  ? Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[

                    Text(
                      "Nothing is here",
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )
                  : GridView.builder(
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                shrinkWrap: true,
                itemCount: posts.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                  childAspectRatio: 300/270,
                ),
                itemBuilder: (BuildContext context, int index) {
                  Post entry = Post.fromJson(posts[index]["item"]);

                  return  Padding(
                      padding: EdgeInsets.all(4),
                      child: Column(

                        children: <Widget>[
                          ListTile(
                            title: Text(entry.title.toString(),
                                style: TextStyle(fontWeight: FontWeight.w800)),
                            onTap: () {

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => InnerPage(todo: entry),
                                  ));
                            },
                          ),
                          GestureDetector(

                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => InnerPage(todo: entry),
                                    ));
                              }, // handle your image tap here
                              child: Image.network(
                                entry.image.toString(),
                               /* width: 300,
                                height: 300,*/
                                fit: BoxFit.contain,
                              )),

                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      ));
                },
              ),
            );
          } else {
            return Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: Text(
                  "Favorites",
                ),
              ),
              body:  Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    /*Image.asset(
                  "assets/images/empty.png",
                  height: 300,
                  width: 300,
                ),
*/
                    Text(
                      "Nothing is here",
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )

            );
          }
        }
    );


  }
}
