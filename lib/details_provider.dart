//
// By ScriptStars, ScriptStars.com

import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/database/favorite_helper.dart';
import 'package:flutter_app/podo/category.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'main.dart';

class DetailsProvider extends ChangeNotifier{
  String message;
  CategoryFeed related = CategoryFeed();
  bool loading = true;
  Post todo;
  var favDB = FavoriteDB();


  bool faved = false;
  bool downloaded = false;

  static var httpClient = HttpClient();


/*
  getFeed(String url) async{
    setLoading(true);
    checkFav(null);
    Api.getCategory(url).then((feed){
      setRelated(feed);
      setLoading(false);
    }).catchError((e){
      throw(e);
    });
  }
*/

  checkFav(Post todo) async{
    print(todo.title);
    List c = await favDB.check({"id": todo.id});
    if(c.isNotEmpty){
      setFaved(true);
    }else{
      setFaved(false);
    }
  }

  addFav(Post todo) async{
    await favDB.add({"id": todo.id,"image":todo.image, "item": todo.toJson()});
    checkFav( todo);
  }

  removeFav(Post todo) async{
    favDB.remove({"id": todo.id}).then((v){
      print(v);
      checkFav(todo);
    });
  }

  void setLoading(value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void setMessage(value) {
    message = value;
    Fluttertoast.showToast(
      msg: value,
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIos: 1,
    );
    notifyListeners();
  }

  String getMessage() {
    return message;
  }

  void setRelated(value) {
    related = value;
    notifyListeners();
  }

  CategoryFeed getRelated() {
    return related;
  }

  void setEntry(value) {
    todo = value;
    notifyListeners();
  }

  void setFaved(value) {
    faved = value;
    notifyListeners();
  }


}