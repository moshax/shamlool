// By ScriptStars, ScriptStars.com

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:page_transition/page_transition.dart';
import 'dart:convert';
import 'home.dart';
import 'contactus.dart';
import 'favorites.dart';
import 'inner.dart';
import 'main.dart';
import 'constants.dart' as Constants;


class SearchPageState extends State<SearchPage> {
  int _page = 1;
  SearchPageState({Key key, this.keyword});
  final String keyword;

  List<Post> data;
  List<Choice> apiCat;
  var topicBaseUrl = Constants.topicBaseUrl;
  String topicUrl = Constants.topicUrl;
  var appBarTitleText = mainAppBarTitleText;
  bool fromSelect = false;

  void _select(Choice choice) {
    fromSelect = true;
    // Causes the app to rebuild with the new _selectedChoice.
    //print(choice.title);
    topicUrl = topicBaseUrl + "&Cat=" + choice.id.toString();
    getData();
    appBarTitleText = Text(choice.title);
    setState(() {
    });
  }

  Future<String> getData() async {
    if (!fromSelect) {
      topicUrl += "&keyword=" + keyword;
    }

    print(topicUrl);
    var response = await http
        .get(Uri.encodeFull(topicUrl), headers: {"Accept": "application/json"});

    setState(() {
      List jsonResponse = json.decode(response.body);
      data = jsonResponse
          .map((job) => new Post.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
    });
    return "Success";
  }

  Future<String> getCategory() async {
    var response = await http.get(
        Uri.encodeFull(Constants.BaseUrl + "?act=category"),
        headers: {"Accept": "application/json"});

    setState(() {
      List jsonResponse = json.decode(response.body);
      apiCat = jsonResponse
          .map((job) => new Choice.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
      //print(apiCat);
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    getData();
    getCategory();
  }

  navigationBaronTap(int index) {
    if (index == 0) {
      //Navigator.pop(context);
      Navigator.pushReplacement(
        context,
        PageTransition(
          type: PageTransitionType.rightToLeft,
          child: MyHomePage(),
        ),
      );
      //_pageController.jumpToPage(0);

    } else {
      Navigator.of(context)
          .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
        if (index == 1) {
          //Fav
          return Favorites();
        } else if (index == 2) {
          return ContactUs();
        }
        return new MyHomePage();
      }));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(title: mainLogo, actions: <Widget>[
          // action button
          IconButton(
            icon: Icon(choices[0].icon),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHomePage(),
                  ));
            },
          ),
          // overflow menu
          PopupMenuButton<Choice>(
            onSelected: _select,
            itemBuilder: (BuildContext context) {
              return apiCat.map((Choice choice) {
                return PopupMenuItem<Choice>(
                  value: choice,
                  child: Text(choice.title),
                );
              }).toList();
            },
          ),
        ]),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _page,
          backgroundColor: Theme.of(context).primaryColor,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white,
          elevation: 20,
          type: BottomNavigationBarType.fixed,
          onTap: navigationBaronTap,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              title: SizedBox(),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.favorite,
              ),
              title: SizedBox(),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.contact_phone,
              ),
              title: SizedBox(),
            ),
          ],
          //onTap: navigationTapped,
          //currentIndex: _page,
        ),
        body: Center(
          child: getList(),
        ));
  }

  Widget getList() {
    if (data == null || data.length < 1) {
      return Container(
        child: Center(
          child: Text("جار التحميل..."),
        ),
      );
    }
    return ListView.separated(
      itemCount: data?.length,
      itemBuilder: (BuildContext context, int index) {
        return getListItem(index);
      },
      separatorBuilder: (context, index) {
        return Divider();
      },
    );
  }

  Widget getListItem(int i) {
    if (data == null || data.length < 1) return null;
    if (i == 0) {
      return Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(4),
            child: Center(
              child: Text(
                keyword,
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            child: Container(
              margin: EdgeInsets.all(8.0),
              child: Padding(
                  padding: EdgeInsets.all(4),
                  child: Column(children: <Widget>[
                    ListTile(
                      title: Text(data[i].title.toString(),
                          style: TextStyle(fontWeight: FontWeight.w800)),
                      onTap: () {
                        print(data.length.toString() + "asd");
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => InnerPage(todo: data[i]),
                            ));
                      },
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => InnerPage(todo: data[i]),
                              ));
                        }, // handle your image tap here
                        child: Image.network(
                          data[i].image.toString(),
                        )),
                  ])),
            ),
          )
        ],
      );
    }

    return Container(
      child: Container(
        margin: EdgeInsets.all(8.0),
        child: Padding(
            padding: EdgeInsets.all(4),
            child: Column(children: <Widget>[
              ListTile(
                title: Text(data[i].title.toString(),
                    style: TextStyle(fontWeight: FontWeight.w800)),
                onTap: () {
                  print("ddd" + data[i].title.toString() + "fff");
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => InnerPage(todo: data[i]),
                      ));
                },
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => InnerPage(todo: data[i]),
                        ));
                  }, // handle your image tap here
                  child: Image.network(
                    data[i].image.toString(),
                  )),
            ])),
      ),
    );
  }
/*
  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }*/

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}