//
// By ScriptStars, ScriptStars.com

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/search.dart';
import 'package:flutter_app/splash.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'favorites_provider.dart';
import 'details_provider.dart';


//import 'package:flutter_app/comment_model.dart';
void main() => runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => DetailsProvider()),
          ChangeNotifierProvider(create: (_) => FavoritesProvider()),
        ],
        child: MyApp(),
      ),
    );

var mainLogo = Row(
  mainAxisAlignment: MainAxisAlignment.start,
  children: [
    Image.asset(
      'assets/Logo.png',
      fit: BoxFit.contain,
      height: 50,
    ),
    Container(padding: const EdgeInsets.all(8.0), child: Text(''))
  ],
);

AnimationController  progressController;
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'شملول',
      theme: ThemeData(
        //primarySwatch: Colors.white,
        primaryColor: Colors.white,
        //accentColor: Colors.deepPurple,
        //secondaryHeaderColor: Colors.deepPurple
      ),
      home: Splash(),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale("ar"), // OR Locale('ar', 'AE') OR Other RTL locales
      ],
      locale: Locale("ar"),
    );
  }
}



class SearchPage extends StatefulWidget {
  SearchPage({Key key,  this.keyword}) : super(key: key);
  final String keyword;

  @override
  SearchPageState createState() => SearchPageState(keyword: keyword);
}

const Text mainAppBarTitleText = const Text("شملول");



class Post {
  final String id;
  final String title;
  final String image;
  final String thumbImage;
  final String link;
  final String abstract;
  final String pubdate;
  final String catname;
  final String catid;
  final String position;
  final List<Video> videos;

  Post(
      {this.id,
      this.title,
      this.image,
      this.thumbImage,
      this.link,
      this.abstract,
      this.pubdate,
      this.catname,
      this.catid,
      this.position,
      this.videos});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
        id: json['id'],
        title: json['title'],
        image: json['image'],
        thumbImage: json['thumbImage'],
        link: json['link'],
        abstract: json['abstract'],
        pubdate: json['pubdate'],
        catname: json['catname'],
        catid: json['catid'],
        position: json['position'],
        videos: json['videos'] == null
            ? null
            : (json['videos'] as List).map((i) => Video.fromJson(i)).toList());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.id != null) {
      data['id'] = this.id;
    }
    if (this.title != null) {
      data['title'] = this.title;
    }

    if (this.pubdate != null) {
      data['pubdate'] = this.pubdate;
    }

    if (this.catname != null) {
      data['catname'] = this.catname;
    }

    if (this.catid != null) {
      data['catid'] = this.catid;
    }

    if (this.abstract != null) {
      data['abstract'] = this.abstract;
    }

    if (this.abstract != null) {
      data['updated'] = this.abstract;
    }

    if (this.image != null) {
      data['image'] = this.image;
    }
    if (this.thumbImage != null) {
      data['thumbImage'] = this.thumbImage;
    }

    if (this.position != null) {
      data['position'] = this.position;
    }

    if (this.videos != null) {
      data['videos'] = this.videos;
    }

    return data;
  }
}

class Video {
  final String id;
  final String title;
  final String filepath;

  Video({
    this.id,
    this.title,
    this.filepath,
  });

  factory Video.fromJson(dynamic json) {
    return Video(
      id: json['id'].toString() ?? "",
      title: json['title'].toString() ?? "",
      filepath: json['filepath'].toString() ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.id != null) {
      data['title'] = this.id;
    }

    if (this.title != null) {
      data['title'] = this.title;
    }

    if (this.filepath != null) {
      data['pubdate'] = this.filepath;
    }

    return data;
  }
}

class Choice {
  const Choice({this.title, this.icon, this.id,this.image});

  final String id;
  final String title;
  final IconData icon;
  final String image;

  factory Choice.fromJson(Map<String, dynamic> json) {
    return Choice(
      id: json['id'],
      title: json['title'],
      icon: json['icon'],
      image: json['image'],
    );
  }
}

const List<Choice> choices = const <Choice>[
  const Choice(id: "0", title: 'الرئيسية', icon: Icons.home),
  const Choice(id: "1", title: 'Bicycle', icon: Icons.directions_bike),
  const Choice(id: "1", title: 'Boat', icon: Icons.directions_boat),
  const Choice(id: "1", title: 'Bus', icon: Icons.directions_bus),
  const Choice(id: "1", title: 'Train', icon: Icons.directions_railway),
  const Choice(id: "1", title: 'Walk', icon: Icons.directions_walk),
];

 Color fromHex(String hexString) {
final buffer = StringBuffer();
if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
buffer.write(hexString.replaceFirst('#', ''));
return Color(int.parse(buffer.toString(), radix: 16));
}