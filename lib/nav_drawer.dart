// By ScriptStars, ScriptStars.com

import 'package:flutter/material.dart';
import 'package:flutter_app/contactus.dart';
import 'package:page_transition/page_transition.dart';
import 'inner.dart';
import 'main.dart';
import 'outer.dart';
import 'package:url_launcher/url_launcher.dart';


class NavDrawer extends StatelessWidget {
  final List<Post> data;

  final int _page = 3;
  NavDrawer({Key key, this.data});

  onTap(context, String catId) {
    if (catId == "0") {
      Navigator.pushReplacement(
        context,
        PageTransition(
          type: PageTransitionType.rightToLeft,
          child: ContactUs(),
        ),
      );
    } else if (catId == "85") {
      Post post = data.firstWhere((element) => element.id=="53");
      Navigator.push(
        context,
        PageTransition(
          type: PageTransitionType.rightToLeft,
          child: InnerPage(
            todo: post,
          ),
        ),
      );
    } else if (catId == "-1") {
      var aboutNews = new Post();
      print(data.length.toString()+"asd");
      if (data.length > 0) {
        for (var item in data) {
          if (item.id == "11") {
            aboutNews = item;
          }
        }
      }

      Navigator.push(
        context,
        PageTransition(
          type: PageTransitionType.rightToLeft,
          child: InnerPage(
            todo: aboutNews,
          ),
        ),
      );
    } else {
      //print(catId+"abc");
      Navigator.push(
        context,
        PageTransition(
          type: PageTransitionType.rightToLeft,
          child: OuterPage(catId: catId),
        ),
      );
    }
  }

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final TextEditingController searchController = new TextEditingController();

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('يجب ادخال كلمة البحث لا تقل عن 3 حروف'),
            actions: <Widget>[
              new FlatButton(
                child: new Text('رجوع'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  void _submitForm(context) {
    final FormState form = _formKey.currentState;

    if (searchController.text == "" || searchController.text.length < 3) {
      //showMessage('Form is not valid!  Please review and correct.');
      _displayDialog(context);
    } else {
      //print(searchController.text);
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SearchPage(keyword: searchController.text),
          ));
    }
  }
  _launchURL(String url) async {

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    //print(data.length);
    double screenWidth = MediaQuery.of(context).size.width;
    double width = (screenWidth/5) -30;
    return
      /*Drawer(
      child:*/
      ColoredBox(
        color: Colors.white,
        child: Container(
          width: screenWidth-30,

          child:
          ListView(
            padding: EdgeInsets.only(right: 10),
            children: <Widget>[
              Container(padding: const EdgeInsets.only(top: 35,)),
              Container(
                width: 90.0,
                height: 70.0,
                child: DrawerHeader(
                  padding: const EdgeInsets.all(40.0),
                  decoration: BoxDecoration(
                      color: Colors.white70,
                      image: DecorationImage(
                          fit: BoxFit.contain,
                          image: AssetImage('assets/Logo.png'))),
                ),
              ),
              ExpansionTile(
                title: Text("روبابكيا"),
                leading: Image.asset("assets/robabikia.png", width: 40, height: 40),
                children: <Widget>[
                  ListTile(
                    leading:
                    Image.asset("assets/plastic.png", width: 40, height: 40),
                    title: Text('بلاستيكات'),
                    onTap: () => {onTap(context, "60")},
                  ),
                  ListTile(
                    leading:
                    Image.asset("assets/m3aden.png", width: 40, height: 40),
                    title: Text('معادن'),
                    onTap: () => {onTap(context, "62")},
                  ),  ListTile(
                    leading:
                    Image.asset("assets/khashab.png", width: 40, height: 40),
                    title: Text('خشبيات'),
                    onTap: () => {onTap(context, "61")},
                  ),
                  ListTile(
                    leading:
                    Image.asset("assets/akmesha.png", width: 40, height: 40),
                    title: Text('اقمشة'),
                    onTap: () => {onTap(context, "59")},
                  ),
                  ListTile(
                    leading: Image.asset("assets/warq.png", width: 40, height: 40),
                    title: Text('ورقيات'),
                    onTap: () => {onTap(context, "63")},
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("روشيته"),
                leading: Image.asset("assets/roshita.png", width: 40, height: 40),
                children: <Widget>[
                  ListTile(
                    leading: Image.asset("assets/azmat.png", width: 40, height: 40),
                    title: Text(' ازمات نفسية '),
                    onTap: () => {onTap(context, "64")},
                  ),
                  ListTile(
                    leading: Image.asset("assets/esaba.png", width: 40, height: 40),
                    title: Text(' اصابه و نزيف '),
                    onTap: () => {onTap(context, "65")},
                  ),
                  ListTile(
                    leading: Image.asset("assets/e3.png", width: 40, height: 40),
                    title: Text(' اعياء و اغماء '),
                    onTap: () => {onTap(context, "66")},
                  ),
                  ListTile(
                    leading: Image.asset("assets/3elag.png", width: 40, height: 40),
                    title: Text(' علاج طبيعي و اعشاب '),
                    onTap: () => {onTap(context, "67")},
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("فيشه وعجله"),
                leading: Image.asset("assets/fisha.png", width: 40, height: 40),
                children: <Widget>[
                  ListTile(
                    leading:
                    Image.asset("assets/sebaka.png", width: 40, height: 40),
                    title: Text('سباكة'),
                    onTap: () => {onTap(context, "68")},
                  ),
                  ListTile(
                    leading:
                    Image.asset("assets/siarat.png", width: 40, height: 40),
                    title: Text('سيارات'),
                    onTap: () => {onTap(context, "69")},
                  ),
                  ListTile(
                    leading:
                    Image.asset("assets/computer.png", width: 40, height: 40),
                    title: Text('كمبيوتر و انترنت '),
                    onTap: () => {onTap(context, "70")},
                  ),
                  ListTile(
                    leading:
                    Image.asset("assets/kahraba.png", width: 40, height: 40),
                    title: Text('كهرباء'),
                    onTap: () => {onTap(context, "71")},
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("مدخل وسطوح"),
                leading: Image.asset("assets/sotoh.png", width: 40, height: 40),
                children: <Widget>[
                  ListTile(
                    leading:
                    Image.asset("assets/dekorat.png", width: 40, height: 40),
                    title: Text('ديكورات'),
                    onTap: () => {onTap(context, "72")},
                  ),
                  ListTile(
                    leading:
                    Image.asset("assets/rsomat.png", width: 40, height: 40),
                    title: Text('رسومات'),
                    onTap: () => {onTap(context, "73")},
                  ),
                  ListTile(
                    leading:
                    Image.asset("assets/manzel.png", width: 40, height: 40),
                    title: Text('نباتات منزلية'),
                    onTap: () => {onTap(context, "74")},
                  ),

                ],
              ),
              ListTile(
                leading: Icon(Icons.grid_on),
                title: Text('دليل شملول '),
                onTap: () => {onTap(context, "85")},
              ),
              ListTile(
                leading: Icon(Icons.contact_mail),
                title: Text('اتصل بنا'),
                onTap: () => {onTap(context, "0")},
              ),
              ListTile(
                leading: Icon(Icons.info),
                title: Text(' من نحن'),
                onTap: () => {onTap(context, "-1")},
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 50,),

                  Divider(),
                  Container(
                      color: Colors.white,
                      width: width,
                      child: Column(
                        children: <Widget>[
                          new Container(
                              height: 35,
                              width: width,
                              child: Container(
                                width: width,
                                height: 35.0,
                                child: GestureDetector(
                                    onTap: () {
                                      _launchURL("https://www.facebook.com/Shamlool-113897810262230/");
                                    }, // handle your  tap here
                                    child: Image.asset(
                                      "assets/facebook-48.png",
                                      fit: BoxFit.contain,
                                      width: 35,
                                      height: 35,
                                    )),
                              )),

                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      )),
                  Container(
                      color: Colors.white,
                      child: Column(
                        children: <Widget>[
                          new Container(

                              height: 35,
                              child: Container(
                                width: width,
                                height: 35.0,
                                child: GestureDetector(
                                    onTap: () {
                                      _launchURL("https://twitter.com/Shamlool10");
                                    }, // handle your  tap here
                                    child: Image.asset(
                                      "assets/twitter-48.png",

                                      fit: BoxFit.contain,
                                      width: 35,
                                      height: 35,
                                    )),
                              )),

                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      )),
                  Container(
                      color: Colors.white,
                      child: Column(
                        children: <Widget>[
                          new Container(

                              height: 35,
                              child: Container(
                                width: width,
                                height: 35.0,
                                child: GestureDetector(
                                    onTap: () {
                                      _launchURL("https://www.youtube.com/channel/UCWhy17ayxawLDO0PVy7Zscg");
                                    }, // handle your  tap here
                                    child: Image.asset(
                                      "assets/youtube-squared-48.png",
                                      fit: BoxFit.contain,
                                      width: 35,
                                      height: 35,
                                    )),
                              )),

                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      )),
                  Container(
                      color: Colors.white,
                      child: Column(
                        children: <Widget>[
                          new Container(

                              height: 35,
                              child: Container(
                                width: width,
                                height: 35.0,
                                child: GestureDetector(
                                    onTap: () {
                                      _launchURL("https://vm.tiktok.com/JLWcUwe/");
                                    }, // handle your  tap here
                                    child: Image.asset(
                                      "assets/tiktok-48.png",
                                      fit: BoxFit.contain,
                                      width: 35,
                                      height: 35,
                                    )),
                              )),

                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      )),
                  Container(
                      color: Colors.white,
                      child: Column(
                        children: <Widget>[
                          new Container(

                              height: 35,
                              child: Container(
                                width: width,
                                height: 35.0,
                                child: GestureDetector(
                                    onTap: () {
                                      _launchURL("https://www.instagram.com/shamlool.app/?hl=en");
                                    }, // handle your  tap here
                                    child: Image.asset(
                                      "assets/icons8-instagram-48.png",
                                      fit: BoxFit.contain,
                                      width: 35,
                                      height: 35,
                                    )),
                              )),

                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      )),
                ],
              )
/*          Container(
            margin: EdgeInsets.only(left: 16.0),
            child:
            TextFormField(
              controller: searchController,
              decoration: InputDecoration(
                  hintText: 'كلمة البحث',
                  filled: true,
                  prefixIcon: Icon(
                    Icons.title,
                    size: 28.0,
                  ),
                  suffixIcon: IconButton(
                    color: Colors.black,
                      icon: Icon(Icons.search),
                      onPressed: () {
                        _submitForm(context);
                      })),
            ),
          )*/
            ],
          ),

        ),
      );

    //);
  }
}
