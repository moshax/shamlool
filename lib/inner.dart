//
// By ScriptStars, ScriptStars.com

import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/database/favorite_helper.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:share/share.dart';
import 'package:webview_flutter/webview_flutter.dart' as webview_flutter;
import 'Favorites.dart';
import 'home.dart';
import 'common.dart';
import 'contactus.dart';
import 'details_provider.dart';
import 'main.dart';
import 'constants.dart' as Constants;
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class _InnerPageState extends State<InnerPage> {
  List<Post> data;
  final Post todo;
  int _page = 3;
  _InnerPageState({Key key, @required this.todo});

  List<Choice> apiCat;
  var topicBaseUrl = Constants.topicBaseUrl;
  String topicUrl = Constants.topicUrl;
  var appBarTitleText = mainAppBarTitleText;

  var myDetailsProvider = new DetailsProvider();
  var favDB = FavoriteDB();

  Future<String> getData() async {
    //print(topicUrl);
    var response = await http
        .get(Uri.encodeFull(topicUrl), headers: {"Accept": "application/json"});

    setState(() {
      List jsonResponse = json.decode(response.body);
      data = jsonResponse
          .map((job) => new Post.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
    });

    return "Success";
  }

  Future<String> getCategory() async {
    var response = await http.get(
        Uri.encodeFull("https://shamlool.2ooly.com/Api.php?act=category"),
        headers: {"Accept": "application/json"});

    setState(() {
      List jsonResponse = json.decode(response.body);
      apiCat = jsonResponse
          .map((job) => new Choice.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
    });
    return "Success";
  }

  Future<String> getFav() async {
    setState(() {
      var c = favDB.check({"id": todo.id}).then((c) {
        if (c.isNotEmpty) {
          myDetailsProvider.setFaved(true);
        } else {
          myDetailsProvider.setFaved(false);
        }
      }).catchError((e) {
        throw (e);
      });
    });
    return "Success";
  }

  Future<void> share() async {
    //todo.link
    await Share.share("https://www.facebook.com/Shamlool-App-113897810262230/");
  }

  @override
  void initState() {
    myDetailsProvider.todo = todo;
    super.initState();
    getData();
    getCategory();
    getFav();
  }

  navigationBaronTap(int index, int page) {
    if (index == 0) {
      Navigator.pop(context);

    } else {
      Navigator.of(context)
          .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
        if (index == 1) {
          //Fav
          return Favorites();
        } else if (index == 2) {
          return ContactUs();
        }
        return new MyHomePage();
      }));
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> wdgtList = new List<Widget>();
    print(todo.videos.length);
    if (todo.videos.length > 0) {
      for (Video video in todo.videos) {
        var player = Container(
            width: 300,
            height: 300,
            child: webview_flutter.WebView(
              initialUrl: Uri.dataFromString(
                      '<html><body><iframe src="https://www.youtube.com/embed/' +
                          video.filepath +
                          '"></iframe></body></html>',
                      mimeType: 'text/html')
                  .toString(),
              javascriptMode: webview_flutter.JavascriptMode.unrestricted,
            ));
        wdgtList.add(player);
      }
    }
    bool showImage = true;
    String abstract= todo.abstract;
    if(todo.image==null || todo.image==""){
      showImage=false;
    }
    if(todo.abstract.contains("فيديو*")){
      showImage=false;
      abstract= todo.abstract.replaceAll("فيديو*", "");
    }

    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: mainLogo, //Text(todo.title),
          actions: <Widget>[

            IconButton(
              onPressed: () async {
                List c = await favDB.check({"id": todo.id}).then((c) {
                  if (c.isNotEmpty) {
                    setState(() {
                      myDetailsProvider.setFaved(true);
                      myDetailsProvider.removeFav(todo);
                      myDetailsProvider.faved=false;
                    });


                  } else {
                    setState(() {
                      myDetailsProvider.setFaved(false);
                      myDetailsProvider.addFav(todo);
                      myDetailsProvider.faved=true;
                    });
                  }
                }).catchError((e) {
                  throw (e);
                });
              },
              icon: Icon(
                myDetailsProvider.faved
                    ? Icons.favorite
                    : Icons.favorite_border,
                color: myDetailsProvider.faved
                    ? Colors.red
                    : Theme.of(context).iconTheme.color,
              ),
            ),
            IconButton(

              icon: Icon(Icons.search),
              color: fromHex('#3685cf'),
              onPressed: () {
                Common common = new Common();
                common.displayDialog(context,0);
              },
            ),

          ],
        ),

        bottomNavigationBar:
        FFNavigationBar(
          theme: FFNavigationBarTheme(
              barBackgroundColor: Colors.white,
              selectedItemBorderColor: Colors.blue,
              selectedItemBackgroundColor: fromHex('#3685cf'),
              selectedItemIconColor: Colors.white,
              selectedItemLabelColor: Colors.black,
              unselectedItemIconColor: fromHex('#3685cf'),
              barHeight: 50),
          selectedIndex: _page,
          onSelectTab: (index) {
            setState(() {
              _page = index;
            });
            navigationBaronTap(index, _page);
          },
          items: [
            FFNavigationBarItem(
              iconData: Icons.home,
              label: 'الرئيسية',
            ),
            FFNavigationBarItem(
              iconData: Icons.favorite,
              label: 'المفضلة',
            ),
            FFNavigationBarItem(
              iconData: Icons.contact_phone,
              label: 'اتصل بنا',
            ),
          ],
        ),
        body: SingleChildScrollView(
            child: Stack(children: <Widget>[
          //padding: EdgeInsets.all(16.0),
          Container(
              padding: EdgeInsets.all(3.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    child: Card(
                      child: Column(
                        children: [
                          ListTile(
                            title: Text(todo.title,
                                style: TextStyle(fontWeight: FontWeight.w800)),
                            subtitle: todo.catname==todo.title?Text("") : Text(todo.catname),
                            /*leading: Icon(
                              Icons.apps,
                              color: Colors.blue[500],
                            ),*/
                          ),
                          Divider(),
                          showImage?
                          Image.network(
                            todo.image.toString(),
                          ): Text(""),

                          SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            padding: const EdgeInsets.all(3.0),
                            child:

                            HtmlWidget(
                              abstract,
                              webView: true,
                            )
                            ,
                          ),
                          Row(
                            children: wdgtList,
                          ),
                          FlatButton(
                            child: IconButton(icon: Icon(Icons.share)),
                            onPressed: share,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ))
        ])));
  }
}


class InnerPage extends StatefulWidget {
  InnerPage({Key key, this.todo}) : super(key: key);
  final Post todo;

  @override
  _InnerPageState createState() => _InnerPageState(todo: todo);
}
