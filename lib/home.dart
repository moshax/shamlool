import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'Favorites.dart';
import 'common.dart';
import 'contactus.dart';
import 'constants.dart' as Constants;
import 'inner.dart';
import 'main.dart';
import 'nav_drawer.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'outer.dart';



class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin{
  PageController _pageController;
  int _page = 0;
  List<Post> homeData;
  List<Choice> apiCat;

  var topicBaseUrl = Constants.topicBaseUrl;
  var topicUrl = Constants.topicUrl+"&Limit=100";
  var appBarTitleText = mainAppBarTitleText;


  Future<String> getData() async {
    //print("home "+topicUrl);
    var response = await http
        .get(Uri.encodeFull(topicUrl), headers: {"Accept": "application/json"});

    setState(() {
      List jsonResponse = json.decode(response.body);
      homeData = jsonResponse
          .map((job) => new Post.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
    });
    return "Success";
  }

  Future<String> getCategory() async {
    var response = await http.get(
        Uri.encodeFull(Constants.BaseUrl + "?act=category"),
        headers: {"Accept": "application/json"});

    setState(() {
      List jsonResponse = json.decode(response.body);
      apiCat = jsonResponse
          .map((job) => new Choice.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
      //print(apiCat);
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    getData();
    getCategory();
    _pageController = PageController(initialPage: 0);
    progressController=AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 900),
      reverseDuration: Duration(milliseconds: 900),
    );
  }
/*
  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }*/

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
  TextEditingController searchController = new TextEditingController();
  navigationBaronTap(int index, int page) {
    if (index == 0) {
      if (page > 0) {
        Navigator.pop(context);
      }
    } else {
      Navigator.of(context)
          .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
        if (index == 1) {
          //Fav
          return Favorites();
        } else if (index == 2) {
          return ContactUs();
        }
        return new MyHomePage();
      }));
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavDrawer(data: homeData,),
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(title: mainLogo, actions: <Widget>[
          // action button
          IconButton(
            icon: Icon(Icons.search),
            color: fromHex('#3685cf'),
            onPressed: () {
              Common common = new Common();
              common.displayDialog(context,0);
            },
          ),
          IconButton(
            icon: Icon(choices[0].icon),
            color: fromHex('#3685cf'),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHomePage(),
                  ));
            },
          ),

        ]
        ),
        bottomNavigationBar:
        FFNavigationBar(
          theme: FFNavigationBarTheme(
              barBackgroundColor: Colors.white,
              selectedItemBorderColor: Colors.blueAccent,
              selectedItemBackgroundColor: fromHex('#3685cf'),
              selectedItemIconColor: Colors.white,
              selectedItemLabelColor: Colors.black,
              unselectedItemIconColor: fromHex('#3685cf'),
              barHeight: 50),
          selectedIndex: _page,
          onSelectTab: (index) {
            setState(() {
              _page = index;
            });
            navigationBaronTap(index, _page);
          },
          items: [
            FFNavigationBarItem(
              iconData: Icons.home,
              label: 'الرئيسية',
            ),
            FFNavigationBarItem(
              iconData: Icons.favorite,
              label: 'المفضلة',
            ),
            FFNavigationBarItem(
              iconData: Icons.contact_phone,
              label: 'اتصل بنا',
            ),
          ],
        )

        ,
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: _pageController,
          onPageChanged: onPageChanged,
          children: <Widget>[
            getList(),
            OuterPage(),
            InnerPage(),
          ],
        ));
  }

  Widget getList() {
    if (homeData == null || homeData.length < 1) {
      return Container(
        child: Center(
          child: Text("Please wait..."),
        ),
      );
    }
    return ListView.separated(
      itemCount: homeData?.length,
      itemBuilder: (BuildContext context, int index) {
        return getListItem(index);
      },
      separatorBuilder: (context, index) {
        return Divider();
      },
    );
  }
  _launchURL(String url) async {

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  Widget getListItem(int i) {
    var mainNews = new List<Post>();
    for (var item in homeData) {
      if (item.position == "7") {
        mainNews.add(item);
      }
    }
    List<Widget> wdgtList = new List<Widget>();
    var videoNews = new List<Post>();

    for (var item in homeData) {
      if (item.catid == "56") {
        videoNews.add(item);
        wdgtList.add(videoListBuilder(i, item));
      }
    }

    Widget videoList = Row(
      children: wdgtList,
    );

    List<Widget> wdgtListCorona = new List<Widget>();
    int coronaListCount=1;
    for (var item in homeData) {
      if (item.catid == "82" && coronaListCount < 3) {
        videoNews.add(item);
        wdgtListCorona.add(coronaListBuilder(i, item));
        coronaListCount++;
      }
    }
    Widget coronaList = Row(
      children: wdgtListCorona,
    );
    double screenWidth = MediaQuery.of(context).size.width;
    double width = (screenWidth/4) -20;
    Widget catList =
    Row(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(right: 20.0),
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Container(
                    padding: const EdgeInsets.all(3.0),
                    height: 80,
                    child: Container(
                      width: width,
                      height: 120.0,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => OuterPageMain(catId: "1"),
                                )
                            );
                          }, // handle your  tap here
                          child: Image.asset(
                            "assets/robabikia.png",
                            fit: BoxFit.contain,
                            width: 200.0,
                            height: 130,
                            alignment: Alignment.center,
                          )),
                    )),
                Container(
                  width: width,
                  padding: const EdgeInsets.only(right:7),
                  child: Text("روبابيكيا",textAlign: TextAlign.center),

                )
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),
        Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                    height: 80,
                    padding: const EdgeInsets.all(6.0),
                    child: Container(
                      width: width,
                      height: 120.0,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => OuterPageMain(catId: "22"),
                                ));
                          }, // handle your  tap here
                          child: Image.asset(
                            "assets/fisha.png",
                            fit: BoxFit.contain,
                            width: 80,
                            height: 80,
                          )),
                    )),
                Container(
                  width: width,
                  padding: const EdgeInsets.only(right:1),
                  child: Text("فيشة وعجلة",textAlign: TextAlign.center),

                )
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),
        Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                    padding: const EdgeInsets.all(6.0),
                    height: 80,
                    child: Container(
                      width: width,
                      height: 120.0,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => OuterPageMain(catId: "58"),
                                ));
                          }, // handle your  tap here
                          child: Image.asset(
                            "assets/roshita.png",

                            fit: BoxFit.contain,
                            width: 80,
                            height: 80,
                          )),
                    )),
                Container(
                  width: width,
                  padding: const EdgeInsets.only(right:5),
                  child: Text("روشيته",textAlign: TextAlign.center),

                )
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),
        Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                    padding: const EdgeInsets.all(6.0),
                    height: 80,
                    child: Container(
                      width: width,
                      height: 100.0,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => OuterPageMain(catId: "50"),
                                ));
                          }, // handle your  tap here
                          child: Image.asset(
                            "assets/sotoh.png",
                            fit: BoxFit.contain,
                            width: 80,
                            height: 80,
                          )),
                    )),
                Container(
                  width: width+10,
                  padding: const EdgeInsets.only(right:1),
                  child: Text("مدخل وسطوح",textAlign: TextAlign.center,),

                )
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),


      ],
    );

    Widget social = Row(

      children: <Widget>[

        Divider(),

        Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                    height: 48,
                    padding: const EdgeInsets.all(1.0),
                    child: Container(
                      width: width,
                      height: 48.0,
                      child: GestureDetector(
                          onTap: () {
                            _launchURL("https://www.facebook.com/Shamlool-113897810262230/");
                          }, // handle your  tap here
                          child: Image.asset(
                            "assets/facebook-48.png",
                            fit: BoxFit.contain,
                            width: 48,
                            height: 48,
                          )),
                    )),

              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),
        Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                    padding: const EdgeInsets.all(1.0),
                    height: 48,
                    child: Container(
                      width: width,
                      height: 48.0,
                      child: GestureDetector(
                          onTap: () {
                            _launchURL("https://twitter.com/Shamlool10");
                          }, // handle your  tap here
                          child: Image.asset(
                            "assets/twitter-48.png",

                            fit: BoxFit.contain,
                            width: 48,
                            height: 48,
                          )),
                    )),

              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),
        Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                    padding: const EdgeInsets.all(1.0),
                    height: 48,
                    child: Container(
                      width: width,
                      height: 48.0,
                      child: GestureDetector(
                          onTap: () {
                            _launchURL("https://www.youtube.com/channel/UCWhy17ayxawLDO0PVy7Zscg");
                          }, // handle your  tap here
                          child: Image.asset(
                            "assets/youtube-squared-48.png",
                            fit: BoxFit.contain,
                            width: 48,
                            height: 48,
                          )),
                    )),

              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),
        Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                    padding: const EdgeInsets.all(1.0),
                    height: 48,
                    child: Container(
                      width: width,
                      height: 48.0,
                      child: GestureDetector(
                          onTap: () {
                            _launchURL("https://vm.tiktok.com/JLWcUwe/");
                          }, // handle your  tap here
                          child: Image.asset(
                            "assets/tiktok-48.png",
                            fit: BoxFit.contain,
                            width: 48,
                            height: 48,
                          )),
                    )),

              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),
        Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                    padding: const EdgeInsets.all(1.0),
                    height: 48,
                    child: Container(
                      width: width,
                      height: 48.0,
                      child: GestureDetector(
                          onTap: () {
                            _launchURL("https://www.instagram.com/shamlool.app/?hl=en");
                          }, // handle your  tap here
                          child: Image.asset(
                            "assets/icons8-instagram-48.png",
                            fit: BoxFit.contain,
                            width: 48,
                            height: 48,
                          )),
                    )),

              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),
      ],
    );
    if (homeData == null || homeData.length < 1) return null;

    //print(mainNews.length);
    /*if (mainNews.length == 0) {
      mainNews = homeData;
    }*/

    if (i == 0) {
      //progressController.forward();
      return Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.all(5.0),
              padding: EdgeInsets.all(5.0),
              alignment: Alignment.topCenter,
              child: Center(
                // Slider
                child:
                CarouselSlider(
                  options: CarouselOptions(
                    aspectRatio: 1.6,
                    height: 270,
                    enlargeCenterPage: true,
                    scrollDirection: Axis.horizontal,
                    autoPlay: true,
                    //      autoPlayInterval: Duration(seconds: 3),
                    //      autoPlayAnimationDuration: Duration(milliseconds: 800),
                  ),
                  items:  mainNews.map((i) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.symmetric(horizontal: 5.0),
                            //decoration: BoxDecoration(color: Colors.amber),
                            child: Padding(
                                padding: EdgeInsets.all(3),
                                child: Column(children: <Widget>[
                                  GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  InnerPage(todo: i),
                                            ));
                                      }, // handle your  tap here
                                      child: Image.network(
                                        i.image.toString(),
                                        fit: BoxFit.cover,
                                        width: 350.0,
                                        height: 130,
                                      )),
                                  ListTile(
                                    title: Text(i.title.toString(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400,fontSize: 15)),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                InnerPage(todo: i),
                                          ));
                                    },
                                  )
                                ])));
                      },
                    );
                  }).toList(),
                ),
              )),
          Divider(),

          Container(
            padding: EdgeInsets.only(right: 17),
            child: Row(
              children: <Widget>[
                coronaList
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.all(1.0),
            alignment: Alignment.topCenter,
            child: videoList,
          ),
          catList,
          SizedBox(height: 20),
          social
        ],
      );
    }

  }

  Widget videoListBuilder(int i, Post videoNews) {
    double width = MediaQuery.of(context).size.width;
    double blockWidth = (width/3)-20;
    return Card(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => InnerPage(todo: videoNews),
                    ));
              },
              child: new Container(
                  padding: const EdgeInsets.all(3.0),
                  child: Container(
                      width: blockWidth,
                      height: 80.0,
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          image: new Image.network(videoNews.image).image,
                        ),
                      ),
                      child:
                      //Image.network("https://via.placeholder.com/100C/O"),
                      Stack(
                        children: <Widget>[
                          new Positioned(
                            /*width: 70,
                            height: 70,*/
                            right: 32.0,
                            bottom: 25.0,
                            child: new Icon(
                              Icons.play_circle_outline,
                              color: Colors.white,
                              size: 35,
                            ),
                          ),
                        ],
                      ))),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => InnerPage(todo: videoNews),
                    ));
              },
              child: new Container(
                width: blockWidth,
                height: 100.0,
                padding: const EdgeInsets.all(3.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      videoNews.title,
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.9), fontSize: 16),
                    ),

                    //Text("asd asd asd "),
                  ],
                ),
              ),
            )
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ));
  }


  Widget coronaListBuilder(int i, Post videoNews) {
    double width = MediaQuery.of(context).size.width;
    double blockWidth = (width/2)-23;

    return Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
            side: new BorderSide(color: Colors.white, width: 0.5)
        ),
        color: Colors.white,

        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => InnerPage(todo: videoNews),
                    ));
              },
              child: new Container(

                  padding: const EdgeInsets.only(right: 1,top: 10),
                  child: Container(
                    width: blockWidth,
                    height: 70.0,
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      //borderRadius:  BorderRadius.circular(30.0),
                      gradient: LinearGradient(
                        colors: [Colors.green,Colors.red],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                      image: new DecorationImage(
                        image: new Image.network(videoNews.image).image,
                      ),
                    ),

                  )
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => InnerPage(todo: videoNews),
                    ));
              },
              child: new Container(
                width: blockWidth,
                height: 110.0,
                padding: const EdgeInsets.all(11.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      videoNews.title,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.9), fontSize: 16),
                    ),

                    //Text("asd asd asd "),
                  ],
                ),
              ),
            )
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ));
  }


}
