import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'home.dart';
import 'contactus.dart';
import 'favorites.dart';
import 'main.dart';

class Common  {
  TextEditingController searchController = new TextEditingController();
  displayDialog(BuildContext context,int type) async {
    String title="";
    if(type==1){
      title="برجاء ادخال كلمة بحث لا تقل عن 3 حروف";
    }else{
      title="بحث";
    }
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Container(
              margin: EdgeInsets.only(left: 16.0),
              child:
              TextFormField(
                controller: searchController,
                decoration: InputDecoration(
                    hintText: '',
                    filled: true,
                    /*prefixIcon: Icon(
                      Icons.title,
                      size: 28.0,
                    ),*/
                    suffixIcon: IconButton(
                        color: Colors.black,
                        icon: Icon(Icons.search),
                        onPressed: () {
                          //_submitForm(context);
                          if (searchController.text=="" || searchController.text.length<3) {
                            //showMessage('Form is not valid!  Please review and correct.');
                            displayDialog(context,1);
                          } else {

                            //print(searchController.text);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SearchPage(keyword: searchController.text),
                                )
                            );
                          }
                        })),
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('رجوع'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  displayCustomDialog(BuildContext context,String title) async {

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),

            actions: <Widget>[
              new FlatButton(
                child: new Text('رجوع'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  navigationBaronTap(int index, int page,BuildContext context) {
    if (index == 0) {
      if (page > 0) {
        Navigator.pop(context);
      }
    } else {
      Navigator.of(context)
          .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
        if (index == 1) {
          //Fav
          return Favorites();
        } else if (index == 2) {
          return ContactUs();
        }
        return new MyHomePage();
      }));
    }
  }
}