// By ScriptStars, ScriptStars.com

import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:page_transition/page_transition.dart';
import 'dart:convert';
import 'CircleButton.dart';
import 'Favorites.dart';
import 'home.dart';
import 'common.dart';
import 'contactus.dart';
import 'inner.dart';
import 'main.dart';
import 'constants.dart' as Constants;
import 'nav_drawer.dart';



class OuterPageMain extends StatefulWidget {
  OuterPageMain({Key key, this.title, this.catId}) : super(key: key);
  final String title;
  final String catId;

  @override
  OuterPageMainState createState() => OuterPageMainState(catId: catId);
}

class OuterPageMainState extends State<OuterPageMain> {
  int _page = 0;
  OuterPageMainState({Key key, this.catId});
  final String catId;
  List<Post> data;
  List<Choice> apiCat;

  var topicBaseUrl = Constants.topicBaseUrl;
  String topicUrl = Constants.topicUrl;
  var appBarTitleText = mainAppBarTitleText;
  bool fromSelect = false;
  bool responseDone = false;


  Future<String> getData() async {
    if (!fromSelect) {
      topicUrl += "&Limit=100";
    }

    print(topicUrl);
    var response = await http
        .get(Uri.encodeFull(topicUrl), headers: {"Accept": "application/json"});

    setState(() {
      List jsonResponse = json.decode(response.body);
      data = jsonResponse
          .map((job) => new Post.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
    });
    print(data.length);
    return "Success";
  }

  Future<String> getCategory() async {
    var response = await http.get(
        Uri.encodeFull(Constants.BaseUrl + "?act=category&parent=" + catId),
        headers: {"Accept": "application/json"});

    setState(() {
      responseDone = true;
      List jsonResponse = json.decode(response.body);
      apiCat = jsonResponse
          .map((job) => new Choice.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
      //print(apiCat);
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    getData();
    getCategory();
  }

  navigationBaronTap(int index, int page) {

    if (index == 0) {

        Navigator.pushReplacement(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            child: MyHomePage(),
          ),
        );

    } else {
      Navigator.of(context)
          .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
        if (index == 1) {
          //Fav
          return Favorites();
        } else if (index == 2) {
          return ContactUs();
        }

        return new MyHomePage();
      }));
    }
  }

  @override
  Widget build(BuildContext context) {
    return
      data==null ?    Scaffold(
      body:
      Center(
        child: Text("Please wait..."),
      ),
    ):
      Scaffold(
        drawer: NavDrawer(data: data,),
        resizeToAvoidBottomPadding: false,
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(title: mainLogo, actions: <Widget>[
          // action button
          IconButton(
            icon: Icon(Icons.search),
            color: Colors.indigo,
            onPressed: () {
              Common common = new Common();
              common.displayDialog(context,0);
            },
          ),
          IconButton(
            color: Colors.indigo,
            icon: Icon(choices[0].icon),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHomePage(),
                  ));
            },
          ),

          // overflow menu
        ]),
        bottomNavigationBar:
        FFNavigationBar(
          theme: FFNavigationBarTheme(
              barBackgroundColor: Colors.white,
              selectedItemBorderColor: Colors.blueAccent,
              selectedItemBackgroundColor: fromHex('#3685cf'),
              selectedItemIconColor: Colors.white,
              selectedItemLabelColor: Colors.black,
              unselectedItemIconColor: fromHex('#3685cf'),
              barHeight: 50),
          selectedIndex: _page,
          onSelectTab: (index) {
            setState(() {
              _page = index;
            });
            navigationBaronTap(index, _page);
          },
          items: [
            FFNavigationBarItem(
              iconData: Icons.home,
              label: 'الرئيسية',
            ),
            FFNavigationBarItem(
              iconData: Icons.favorite,
              label: 'المفضلة',
            ),
            FFNavigationBarItem(
              iconData: Icons.contact_phone,
              label: 'اتصل بنا',
            ),
          ],
        ),
        body: Center(
          child: getList(),

        ));
  }

  Widget videoListBuilder(Choice videoNews) {
    return Column(

      children: <Widget>[
        Center(
          child: Container(
              padding:const EdgeInsets.all(3.0) ,
              color: Theme.of(context).primaryColor,
              height: 180,
              alignment: Alignment.topCenter,

              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                OuterPage(catId: videoNews.id),
                          ));
                    },
                    child: new Container(
                        padding: const EdgeInsets.all(3.0),
                        child: Container(
                          width: 100.0,
                          height: 80.0,
                          decoration: new BoxDecoration(
                            image: new DecorationImage(
                              image: new Image.network(videoNews.image).image,
                            ),
                          ),
                        )),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                OuterPage(catId: videoNews.id),
                          ));
                    },
                    child: new Container(
                      width: 100,
                      padding: const EdgeInsets.all(5.0),
                      child:  Text(
                        videoNews.title,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.9),
                            fontSize: 16),
                      ),
                    ),
                  )
                ],
                crossAxisAlignment: CrossAxisAlignment.start,
              )),
        )
      ],
    );
  }

  Widget getList() {
    List<Widget> wdgtList = new List<Widget>();
    if (responseDone && apiCat.length < 1) {
      return Container(
        child: Center(
          child: Text("عفوا لا يوجد محتوى"),
        ),
      );
    } else {
      if (apiCat == null || apiCat.length < 1) {
        return Container(
          child: Center(
            child: Text("جار التحميل..."),
          ),
        );
      }
    }

    int start = 0;
    int gStart = 0;
    int gEnd = 0;
    int theBreak = 2;
    if(apiCat.length < 5){
      theBreak=1;

    }
    double screenWidth = MediaQuery.of(context).size.width;
    //print(screenWidth ); // 360
    List<Widget> positionedList = new List<Widget>();
    Widget bigCircle =
    new Container(
      width: 400,
      height: 400,
      alignment:Alignment.center,
      decoration: new BoxDecoration(
       /* border: Border.all(
            color: Colors.blue,
            width: 1.5
        ),*/
        color: Color.fromRGBO(250, 250, 250, 0.4), //Theme.of(context).primaryColor
        shape: BoxShape.circle,
      ),
    );

    Widget smallCircle =
    new Positioned(
      width: 280,
      height: 280  ,
     top: 40,
     left: 35,
     child: Container(
       alignment:Alignment.center,
       decoration: new BoxDecoration(
         border: Border.all(
             color: Color.fromRGBO(179, 215, 255,0.6),
             width: 1.5
         ),
         color: Color.fromRGBO(250, 250, 250, 0.4), //Theme.of(context).primaryColor
         shape: BoxShape.circle,
       ),
     ),
    );

    positionedList.add(smallCircle);
    positionedList.add(bigCircle);
    List<Widget> subWdgtList = new List<Widget>();

     List<Dimension> dimension =  <Dimension>[
      //const Dimension(top: 0, left: 120),
       Dimension(top: 139, left: 130),

      const Dimension(top: 10, left: 130),
      const Dimension(top: 140, left: 5),
      const Dimension(top: 140, right: 10),
      const Dimension(top: 270, left: 130),
      const Dimension(top: 260, right: 30),

      const Dimension(top: 0, right: 130),
      const Dimension(top: 115, left: 10),
      const Dimension(top: 115, right: 10),
      const Dimension(top: 240, left: 60),
      const Dimension(top: 240, right: 50),


    ];
    //print(apiCat.length.toString() + "asf");
    double top=0;
    double left=0;
    int theStart=1;
    if(apiCat.length>4){
      theStart = theStart+5;
      dimension[0] = Dimension(top: 125, left: 135);
    }
    Dimension currentDimension = dimension[0];
    positionedList.add(new Positioned(
      child: new CircleButton(onTap: () =>  Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) =>
                MyHomePage(),
          )), icon: "assets/icon.jpg",title: "",),
      top: currentDimension.top,
      left: currentDimension.left,
      bottom: currentDimension.bottom,
      right: currentDimension.right,
    )) ;


    for (var item in apiCat) {

       currentDimension = dimension[theStart];
      if(currentDimension !=null){
        //print(theStart.toString()+"|" +item.title);
        positionedList.add(new Positioned(
          child: new CircleButton(onTap: () =>
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    OuterPage(catId: item.id),
              )), icon: item.image,title: item.title,),
          top: currentDimension.top,
          left: currentDimension.left,
          bottom: currentDimension.bottom,
          right: currentDimension.right,
        )) ;
      }


      top = top+20;
      left = left + 50;
      subWdgtList.add(videoListBuilder(item));

      theStart++;
      if (start == theBreak) {

        gEnd = gStart+theBreak+1;
        wdgtList.add(Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: subWdgtList.sublist(gStart, gEnd),
        ));
        print(gStart);
        print(gEnd);
        gStart = gStart+theBreak+1;
        start = -1;
      }
      start++;
    }

    return new Material(
      //color: Colors.black,
      child:
      new Center(
        child:
        Container(
          width: 360,
          child: new Stack(
            children:positionedList,
          ),
        )
      ),
    );
/*
    if(gEnd <apiCat.length){
      //print("asd"+apiCat.length.toString());
      wdgtList.add(Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: subWdgtList.sublist(gEnd, apiCat.length),
      ));
    }

    if(subWdgtList.length>3){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: wdgtList,
      );
    }else{
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: wdgtList,
      );


    }
 */
    







  }

/*
  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }*/

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}


class OuterPage extends StatefulWidget {
  OuterPage({Key key, this.title, this.catId}) : super(key: key);
  final String title;
  final String catId;
  @override
  _OuterPageState createState() => _OuterPageState(catId: catId);
}

class _OuterPageState extends State<OuterPage> {
  int _page = 1;
  _OuterPageState({Key key, this.catId});
  final String catId;

  List<Post> data;
  List<Choice> apiCat;
  var topicBaseUrl = Constants.topicBaseUrl;
  String topicUrl = Constants.topicUrl;
  var appBarTitleText = mainAppBarTitleText;
  bool fromSelect = false;


  Future<String> getData() async {
    if (!fromSelect) {
      topicUrl += "&Cat=" + catId;
    }

    print(topicUrl);
    var response = await http
        .get(Uri.encodeFull(topicUrl), headers: {"Accept": "application/json"});

    setState(() {
      List jsonResponse = json.decode(response.body);
      data = jsonResponse
          .map((job) => new Post.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
    });
    return "Success";
  }

  Future<String> getCategory() async {
    var response = await http.get(
        Uri.encodeFull(Constants.BaseUrl + "?act=category"),
        headers: {"Accept": "application/json"});

    setState(() {
      List jsonResponse = json.decode(response.body);
      apiCat = jsonResponse
          .map((job) => new Choice.fromJson(job))
          .toList(); //Todo.fromJson(json.decode(response.body));
      //print(apiCat);
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    getData();
    getCategory();
  }

  navigationBaronTap(int index) {
    if (index == 0) {
      //Navigator.pop(context);
      Navigator.pushReplacement(
        context,
        PageTransition(
          type: PageTransitionType.rightToLeft,
          child: MyHomePage(),
        ),
      );
      //_pageController.jumpToPage(0);

    } else {
      Navigator.of(context)
          .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
        if (index == 1) {
          //Fav
          return Favorites();
        } else if (index == 2) {
          return ContactUs();
        }
        return new MyHomePage();
      }));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //drawer: NavDrawer(),
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(title: mainLogo, actions: <Widget>[
          // action button

          IconButton(
            color: fromHex('#3685cf'),
            icon: Icon(Icons.search),

            onPressed: () {
              Common common = new Common();
              common.displayDialog(context,0);
            },
          ),
          IconButton(
            color:fromHex('#3685cf'),
            icon: Icon(choices[0].icon),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHomePage(),
                  ));
            },
          ),
          // overflow menu
          /*PopupMenuButton<Choice>(
            onSelected: _select,
            itemBuilder: (BuildContext context) {
              return apiCat.map((Choice choice) {
                return PopupMenuItem<Choice>(
                  value: choice,
                  child: Text(choice.title),
                );
              }).toList();
            },
          ),*/
        ]),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _page,
          backgroundColor: Theme.of(context).primaryColor,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white,
          elevation: 20,
          type: BottomNavigationBarType.fixed,
          onTap: navigationBaronTap,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              title: SizedBox(),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.favorite,
              ),
              title: SizedBox(),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.contact_phone,
              ),
              title: SizedBox(),
            ),
          ],
          //onTap: navigationTapped,
          //currentIndex: _page,
        ),
        body: Center(
          child: getList(),
        ));
  }

  Widget getList() {
    if (data == null ) {
      return Container(
        child: Center(
          child: Text("جار التحميل..."),
        ),
      );
    }else if( data.length < 1){
      return Container(
        child: Center(
          child: Text("عفوا لا يوجد محتوى حاليا"),
        ),
      );
    }
    return ListView.separated(
      itemCount: data?.length,
      itemBuilder: (BuildContext context, int index) {
        return getListItem(index);
      },
      separatorBuilder: (context, index) {
        return Divider();
      },
    );
  }

  Widget getListItem(int i) {
    if (data == null || data.length < 1) return null;
    if (i == 0) {
      return Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(4),
            child: Center(
              child: Text(
                data[i].catname.toString(),
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            child: Container(
              margin: EdgeInsets.all(8.0),
              child: Padding(
                  padding: EdgeInsets.all(4),
                  child: Column(children: <Widget>[
                    ListTile(
                      title: Text(data[i].title.toString(),
                          style: TextStyle(fontWeight: FontWeight.w800)),
                      onTap: () {
                        //print(data.length.toString() + "asd");
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => InnerPage(todo: data[i]),
                            ));
                      },
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => InnerPage(todo: data[i]),
                              ));
                        }, // handle your image tap here
                        child: Image.network(
                          data[i].image.toString(),
                        )),
                  ])),
            ),
          )
        ],
      );
    }

    return Container(
      child: Container(
        margin: EdgeInsets.all(8.0),
        child: Padding(
            padding: EdgeInsets.all(4),
            child: Column(children: <Widget>[
              ListTile(
                title: Text(data[i].title.toString(),
                    style: TextStyle(fontWeight: FontWeight.w800)),
                onTap: () {
                  //print("ddd" + data[i].title.toString() + "fff");
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => InnerPage(todo: data[i]),
                      ));
                },
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => InnerPage(todo: data[i]),
                        ));
                  }, // handle your image tap here
                  child: Image.network(
                    data[i].image.toString(),
                  )),
            ])),
      ),
    );
  }
/*
  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }*/

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}
